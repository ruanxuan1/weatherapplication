from flask import Flask, render_template
from weather import WeatherReport as wr
from flask_cors import CORS

app = Flask(__name__)
CORS(app, resources=r'/*')


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/weather/<city>')
def weather(city):
    main_func = wr(city)
    info = main_func.get_weather(city)
    return '<br>'.join(info)


if __name__ == '__main__':
    app.run()