import requests
from urllib.parse import quote
import json


class WeatherReport:
    def __init__(self, city):
        self.quote_city = quote(city, encoding='utf-8')
        self.result_info = []

    def get_weather(self, city):
        # 易客云天气API
        # get json response from weather api
        r = requests.get('https://v0.yiketianqi.com/api?unescape=1&version=v61&appid=95994974&appsecret=vbWwEI48&city=%s' % city)
        data = json.loads(r.text)
        print(data)

        try:
            city = data['city']
        except Exception as e:
            errcode = data['errcode']
            errmsg = data['errmsg']
            self.result_info.append("errcode: %s %s" % (errcode, errmsg))
            # print(self.result_info)
            return self.result_info

        date = data['date']
        time = data['update_time']
        wea = data['wea']
        tem = data['tem']
        win = data['win']
        speed = data['win_speed']
        humidity = data['humidity']
        air_level = data['air_level']
        air_tips = data['air_tips']
        alarm_content = data['alarm']['alarm_content']
        self.result_info.append("%s %s %s: %s %s度" % (city, date, time, wea, tem))
        self.result_info.append("%s 风力%s 湿度 %s 空气质量: %s" % (win, speed, humidity, air_level))
        self.result_info.append("%s" % air_tips)
        self.result_info.append("%s" % alarm_content)
        print(self.result_info)

        return self.result_info

